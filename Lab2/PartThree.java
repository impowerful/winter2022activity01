import java.util.Scanner;

public class PartThree
{
    public static void main(String[]args)
    {
        java.util.Scanner scan = new Scanner(System.in);
        AreaComputations areacomp = new AreaComputations();

        System.out.println("Please Input a Side of the Square");
        int side = scan.nextInt();

        System.out.println("Please Input a Length of the Rectangle");
        int length = scan.nextInt();

        System.out.println("Please Input a Width of the Rectangle");
        int width = scan.nextInt();

        System.out.println("The Area of the Square is: " + AreaComputations.areaSquare(side));
        System.out.println("The Area of the Rectangle is: " + areacomp.areaRectangle(length, width));
    }
}