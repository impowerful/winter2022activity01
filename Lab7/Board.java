public class Board
{
    private Die DieOne;
    private Die DieTwo;
    private int[][] closedTiles;
    private final int CONSTANTEND = 2;

    public Board()
    {
        this.DieOne = new Die();
        this.DieTwo = new Die();
        this.closedTiles = new int[6][6];
    }
    public String toString()
    {
        String check = "";

        for(int i = 0; i<this.closedTiles.length; i++)
        {
            for(int j = 0; j<this.closedTiles[i].length; j++)
            {
                check = check + " " + this.closedTiles[i][j];
            }
           check = check + "\n";
        }
        return check;
    }
    public boolean playATurn()
    {
        this.DieOne.roll();
        this.DieTwo.roll();

        System.out.println("The numbers rolled: " + this.DieOne.getPips() + " and " + this.DieTwo.getPips());
    
        boolean isClosed = false;

        if(this.closedTiles[this.DieOne.getPips()-1][this.DieTwo.getPips()-1] == CONSTANTEND)
        {
            isClosed = true;
            System.out.println("The Closed Tile At Position: " + this.DieOne.getPips() + " and " + this.DieTwo.getPips() + " is capped on the tries");
        }
        else
        {
            this.closedTiles[this.DieOne.getPips()-1][this.DieTwo.getPips()-1] += 1;
            System.out.println("The Following Tiles: " + this.DieOne.getPips() + ", " + this.DieTwo.getPips() + " are currently being added");

        }
        
        return isClosed;
    }
}