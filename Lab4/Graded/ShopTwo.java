import java.util.Scanner;

public class ShopTwo
{
    public static void main (String[]args)
    {   
        //initializing constructor
        Scanner scan = new Scanner(System.in);
        CellphoneTwo[] Phone = new CellphoneTwo[4];
        //System.out.println(""); are there to make code look better when printed
        //Loop for creating new objects
        for(int i = 0; i < Phone.length; i++)
        {
            System.out.println("Phone number " + (i + 1));
            System.out.println("");
            System.out.println("Input Operating System of your Phone.");
            String OperatingSys = scan.nextLine();
            System.out.println("");
            System.out.println("Input the Brand of your Phone.");
            String Brand = scan.nextLine();
            System.out.println("");
            System.out.println("Input the maximum Refresh Rate of your Phone.");
            int RefreshRate = Integer.parseInt(scan.nextLine());
            System.out.println("");
            Phone[i] = new CellphoneTwo(OperatingSys,Brand,RefreshRate);       
        }
        //reinput of last phone
        System.out.println("Reinput the last Phone's Operating System");
		Phone[3].setmobileOS(scan.nextLine());
        System.out.println("");
		System.out.println("Reinput the last Phone's Brand");
		Phone[3].setbrand(scan.nextLine());
        System.out.println("");
		System.out.println("Reinput the last Phone's Refresh Rate");
		Phone[3].setrefreshRATE(Integer.parseInt(scan.nextLine()));
        System.out.println("");

        //Printing out last phone Information
        System.out.println("Your Operating System on your device is " + Phone[Phone.length-1].getmobileOS());
        System.out.println("The Brand of your phone is " + Phone[Phone.length-1].getbrand());
        System.out.println("Your screen has a Refresh Rate of " + Phone[Phone.length-1].getrefreshRATE() + "hz");
        Phone[Phone.length-1].betterPHONE();
    }
}