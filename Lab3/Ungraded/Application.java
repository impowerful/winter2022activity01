public class Application
{
    public static void main(String [] args)
    {
        Dolphin dolphinone = new Dolphin();
        Dolphin dolphintwo = new Dolphin();

        dolphinone.habitat = "ocean";
        dolphinone.type = "mammal";
        dolphinone.motion = "swimming";

        dolphintwo.habitat = "sea";
        dolphintwo.type = "fish";
        dolphintwo.motion = "flipping";

        System.out.println(dolphinone.habitat);
        System.out.println(dolphinone.type);
        System.out.println(dolphinone.motion);

        System.out.println(dolphintwo.habitat);
        System.out.println(dolphintwo.type);
        System.out.println(dolphintwo.motion);

        dolphinone.doFlip();
        dolphinone.swimFast();
        dolphintwo.doFlip();
        dolphintwo.swimFast();

        Dolphin[] pod = new Dolphin[3];

        pod[0] = dolphinone;
        pod[1] = dolphintwo;
        pod[2] = new Dolphin();

        pod[2].habitat = "lake";
        pod[2].type = "animal";
        pod[2].motion = "diving";

        System.out.println(pod[0].habitat);
        System.out.println(pod[2].habitat);
    }
}