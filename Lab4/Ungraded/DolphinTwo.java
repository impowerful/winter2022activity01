public class DolphinTwo
{
       private String habitat;
       private String type;
       private String motion;
       private int age;

       public void doFlip()
       {
           System.out.println("I just did a cool flip");
       }
       public void swimFast()
       {
           System.out.println("I can swim really fast");
       }
       public String getHabitat()
       {
           return this.habitat;
       }
       public String getType(){
           return this.type;
        }
       public String getMotion(){
           return this.motion;
        }
        public void getHabitatTwo(String newHabitat)
       {
           this.habitat = newHabitat;
       }
       public DolphinTwo(String habitat, String type, String motion, int age)
       {
           this.habitat = habitat;
           this.type =  type;
           this.motion = motion;
           this.age = age;
       }
        public int getAge()
        {
            return this.age;
        }
        public int growUp()
        {
            return (this.age = age + 1);
        }
}