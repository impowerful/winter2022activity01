public class CellphoneTwo
{
    private String mobileOS;
    private String brand;
    private int refreshRATE;

    public void betterPHONE()
    {
        System.out.println("Samsung is a better phone brand always and forever");
    }
    public String getmobileOS()
    {
        return this.mobileOS;
    }
    public String getbrand()
    {
        return this.brand;
    }
    public int getrefreshRATE()
    {
        return this.refreshRATE;
    }
    public void setmobileOS(String newmobileOS)
    {
        this.mobileOS = newmobileOS;
    }
    public void setbrand(String newbrand)
    {
        this.brand = newbrand;
    }
    public void setrefreshRATE(int newrefreshRATE)
    {
        this.refreshRATE = newrefreshRATE;
    }
    public CellphoneTwo(String mobileOS, String brand, int refreshRATE)
    {
        this.mobileOS = mobileOS;
        this.brand = brand;
        this.refreshRATE = refreshRATE;
    }
}