import java.util.Scanner;

public class Shop
{
    public static void main (String[]args)
    {   
        //initializing constructor
        Scanner scan = new Scanner(System.in);
        Cellphone[] Phone = new Cellphone[4];

        //Loop for creating new objects
        for(int i = 0; i < Phone.length; i++)
        {
            Phone[i] = new Cellphone();
            System.out.println("Phone number " + i);
            System.out.println("");
            System.out.println("Input Operating System of your Phone.");
            Phone[i].mobileOS = scan.nextLine();
            System.out.println("");
            System.out.println("Input the Brand of your Phone.");
            Phone[i].brand = scan.nextLine();
            System.out.println("");
            System.out.println("Input the maximum Refresh Rate of your Phone.");
            Phone[i].refreshRATE = Integer.parseInt(scan.nextLine());
            System.out.println("");
        }

        //Printing out last phone Information
        System.out.println(Phone[Phone.length-1].mobileOS);
        System.out.println(Phone[Phone.length-1].brand);
        System.out.println(Phone[Phone.length-1].refreshRATE + "hz");
        System.out.println("");
        Phone[Phone.length-1].betterPHONE();
    }
}