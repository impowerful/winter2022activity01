public class ApplicationTwo
{
    public static void main(String [] args)
    {
        DolphinTwo dolphinone = new DolphinTwo("ocean","mammal","swimming",4);
        DolphinTwo dolphintwo = new DolphinTwo("sea","fish","flipping",5);

        //dolphinone.getHabitatTwo("ocean2");
        //dolphinone.type = "mammal";
        //dolphinone.motion = "swimming";

        //dolphintwo.getHabitatTwo("sea2");
        //dolphintwo.type = "fish";
        //dolphintwo.motion = "flipping";

        System.out.println(dolphinone.getHabitat());
        System.out.println(dolphinone.getType());
        System.out.println(dolphinone.getMotion());

        System.out.println(dolphintwo.getHabitat());
        System.out.println(dolphintwo.getType());
        System.out.println(dolphintwo.getMotion);

        dolphinone.doFlip();
        dolphinone.swimFast();
        dolphintwo.doFlip();
        dolphintwo.swimFast();

        DolphinTwo[] pod = new DolphinTwo[3];

        pod[0] = dolphinone;
        pod[1] = dolphintwo;
        pod[2] = new DolphinTwo("lake","animal","diving",6);

        //pod[2].getHabitatTwo("lake2");
        //pod[2].type = "animal";
        //pod[2].motion = "diving";

        System.out.println(pod[0].getHabitat());
        System.out.println(pod[2].getHabitat());

        System.out.println(dolphinone.getAge());
        dolphinone.growUp();
        dolphinone.growUp();
        dolphinone.growUp();
        System.out.println(dolphinone.getAge());
    }
}